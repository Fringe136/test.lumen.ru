<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class WeatherController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index(Request $request) {

        $this->validate($request, [
            'lat' => 'required|numeric',
            'lon' => 'required||numeric',
            'date' => 'required|date_format:d.m.Y',
        ]);

        // Если использовать отдельное API для города

        // $responseCity = Http::withHeaders([
        //     'Content-Type'=>'application/json',
        //     'Accept'=>'application/json',
        //     'Authorization'=>'Token 125d52d84518f954c985ef031dd0dc5beed0cf20',
        // ])->get('https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address?lat=55.878&lon=37.653');

        // if ( $responseCity->status() !== 200 ) {
        //     return response()->json('Город не определён.',400);
        // }


        // 2 запроса, т.к. в запросе погоды по дням не возвращается название города

        $responseCity = Http::get('https://api.openweathermap.org/data/2.5/weather?lat='.$request['lat'].'&lon='.$request['lon'].'&appid=c93efb6501d3fc6a123b2c903b1f0e41&units=metric&lang=ru');

        if ( $responseCity->status() !== 200 ) {
            return response()->json('Город не определён.',401);
        }


        $responseWeather = Http::get('https://api.openweathermap.org/data/2.5/onecall/timemachine?lat='.$request['lat'].'&lon='.$request['lon'].'&dt='.strtotime($request['date']).'&appid=c93efb6501d3fc6a123b2c903b1f0e41&units=metric&lang=ru');

        if ( $responseWeather->status() !== 200 ) {
            return response()->json($responseWeather->json()['message'],401);
        } 
        
        $out = [
            'city'=>$responseCity->json()['name'],
            'weather'=>$responseWeather->json()['current']['temp'] // либо массив со всей информацией погоды
        ];

        return response()->json($out,200);
    }

}
